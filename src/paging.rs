use crate::arch::paging::{Entry, Page, PageDirectory, PageTable, ENTRIES};
use crate::interrupts::{IOAPIC, LAPIC};

pub const KERNEL_MEM_BEGIN: usize = 0x100000;
pub const KERNEL_MEM_END: usize = 0x2000000;

const KERNEL_NUM_PAGES: usize = KERNEL_MEM_END / Page::SIZE;
const KERNEL_NUM_PTS: usize = KERNEL_NUM_PAGES / ENTRIES;

/// Remapped apic base address
pub const APIC_BASE: usize = KERNEL_MEM_END - 2 * Page::SIZE;
/// Remapped ioapic base address
pub const IOAPIC_BASE: usize = KERNEL_MEM_END - Page::SIZE;

/// Static page tables
static mut TABLES: [PageTable; KERNEL_NUM_PTS] = {
    const TABLE: PageTable = PageTable::new();
    [TABLE; KERNEL_NUM_PTS]
};
static mut PAGE_DIRECTORY: PageDirectory = PageDirectory::new();

pub fn setup_paging() -> &'static PageDirectory {
    // identity map kernel space

    use core::sync::atomic::Ordering;

    unsafe {
        for (pdi, pt) in TABLES.iter_mut().enumerate() {
            PAGE_DIRECTORY.entries[pdi] = Entry::kernel(pt as *const _ as usize);
            for (pti, pte) in pt.entries.iter_mut().enumerate() {
                *pte = Entry::kernel((pdi * ENTRIES + pti) * Page::SIZE);
            }
        }

        // unmap null frame
        TABLES[0].entries[0] = Entry::new();

        // map apic
        PAGE_DIRECTORY
            .map(APIC_BASE, LAPIC.base.load(Ordering::Acquire) as _)
            .unwrap();
        LAPIC.base.store(APIC_BASE as _, Ordering::Release);

        PAGE_DIRECTORY
            .map(IOAPIC_BASE, LAPIC.base.load(Ordering::Acquire) as _)
            .unwrap();
        IOAPIC.base.store(IOAPIC_BASE as _, Ordering::Release);

        &PAGE_DIRECTORY
    }
}

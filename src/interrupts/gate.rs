//! Gates are interrupt handlers for the prologue/epilogue model.

use core::{
    cell::UnsafeCell,
    sync::atomic::{AtomicBool, AtomicU32, Ordering},
};

use crate::scheduler;

static mut EPILOG: AtomicU32 = AtomicU32::new(0);
static mut EPILOGS: [Epilog; 2] = [
    Epilog {
        enqueued: AtomicBool::new(false),
        next: AtomicU32::new(0),
        gate: UnsafeCell::new(Gate::Timer),
    },
    Epilog {
        enqueued: AtomicBool::new(false),
        next: AtomicU32::new(0),
        gate: UnsafeCell::new(Gate::Keyboard { key: '\0' }),
    },
];

/// ISR Primitive for the prolog/epilog model.
pub struct Epilog {
    enqueued: AtomicBool,
    next: AtomicU32,
    gate: UnsafeCell<Gate>,
}

impl Epilog {
    pub fn push(gate: Gate) -> bool {
        let idx = match gate {
            Gate::Timer => 0,
            Gate::Keyboard { .. } => 1,
        };

        let epilog = unsafe { &EPILOGS[idx] };

        if epilog
            .enqueued
            .compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst)
            .is_err()
        {
            return false;
        }

        unsafe { *epilog.gate.get() = gate };

        let head = unsafe { &EPILOG };
        let mut head_val = head.load(Ordering::SeqCst);
        epilog.next.store(head_val, Ordering::SeqCst);
        while let Err(v) =
            head.compare_exchange(head_val, idx as u32 + 1, Ordering::SeqCst, Ordering::SeqCst)
        {
            head_val = v;
            epilog.next.store(head_val, Ordering::SeqCst);
        }
        true
    }
    pub fn pop() -> Option<Gate> {
        let head = unsafe { &EPILOG };

        let mut start = head.load(Ordering::SeqCst);
        loop {
            if start == 0 {
                return None;
            };
            let next = unsafe { &EPILOGS[start as usize - 1] }
                .next
                .load(Ordering::SeqCst);
            match head.compare_exchange(start, next, Ordering::SeqCst, Ordering::SeqCst) {
                Ok(old) => {
                    debug_assert!(old != 0 && old <= unsafe { EPILOGS.len() as _ });
                    let epilog = unsafe { &EPILOGS[old as usize - 1] };
                    let gate = unsafe { (&*epilog.gate.get()).clone() };
                    epilog.enqueued.store(false, Ordering::SeqCst);
                    return Some(gate);
                }
                Err(s) => start = s,
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Gate {
    Timer,
    Keyboard { key: char },
}

impl Gate {
    pub fn epilog(&mut self) {
        match self {
            Gate::Timer => timer(),
            Gate::Keyboard { key } => keyboard(*key),
        }
    }
}

fn timer() {
    scheduler::resume();
}

fn keyboard(key: char) {
    kprint!("{key}");
}

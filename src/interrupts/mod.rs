#[derive(Debug, Clone, Copy)]
pub enum Vector {
    Timer = 32,
    Keyboard = 33,
    Serial = 34,
    Mouse = 44,
    Syscall = 80,
    Panic = 99,
    Assassin = 100,
    WakeUp = 101,
    Spurious = 255,
}

use core::num::NonZeroUsize;

use crate::arch::acpi::{Acpi, ApicEntry, MulAcpiTable};
use crate::arch::int::lapic::Device;
use crate::arch::int::pic;
use crate::arch::int::{ioapic::IoApic, lapic::LApic};
use crate::CPUS;

pub static LAPIC: LApic = LApic::new();
pub static IOAPIC: IoApic = IoApic::new();

const INVALID_ID: u8 = 0xff;

mod gate;
pub mod guard;
mod idt;

/// Initialize the Interrupt Descriptor Table and Interrupt Controllers.
pub fn init(acpi: Acpi) {
    idt::init();

    pic::init(pic::PicPort::NONE);

    let madt = acpi.find(*b"APIC").unwrap().cast::<MulAcpiTable>();
    let flags = madt.flags;
    let mut lapic_base = madt.lapic_address;

    // The APIC operating mode is set to compatible PIC mode - we have to change it.
    if flags.pcat_compat() {
        log!("disable pic");
        pic::disable();
    }

    log!("lapic base: {lapic_base}");

    let mut lapics = 0;
    let mut lapic_ids = [INVALID_ID; CPUS];
    let mut ioapic = None;
    let mut slot_map = [0, 16];

    for (i, slot) in slot_map.iter_mut().enumerate() {
        *slot = i as _;
    }

    for mads in madt.iter() {
        match mads {
            ApicEntry::LAPIC(mads) => {
                let flags = mads.flags;
                if flags.enabled() && mads.apic_id != INVALID_ID && lapics < lapic_ids.len() {
                    lapic_ids[lapics] = mads.apic_id;
                    lapics += 1;
                } else {
                    log!("ignoreing {mads:?}");
                }
            }
            ApicEntry::IOAPIC(mads) => {
                if mads.global_int_base < IoApic::SLOT_MAX as _ {
                    ioapic = Some(mads)
                } else {
                    log!("ignoreing ioapic as we only support one");
                }
            }
            ApicEntry::IntSrcOverride(mads) => {
                if mads.bus == 0 {
                    let global_int = mads.global_int;
                    let source = mads.source as usize;
                    log!("Override interrupt {source} <- {global_int}");
                    if source < slot_map.len() {
                        slot_map[source as usize] = global_int as _;
                    }
                } else {
                    log!("invalid apic bus {}", mads.bus);
                }
            }
            ApicEntry::LAPICAddrOverride(mads) => {
                lapic_base = mads.lapic_addr as _;
                log!("override LAPIC base {lapic_base}");
            }
            _ => {
                log!("ignore {mads:?}")
            }
        }
    }

    let ioapic_base = ioapic.and_then(|a| NonZeroUsize::new(a.ioapic_addr as _));
    let ioapic_id = ioapic.map(|a| a.ioapic_id as _).unwrap_or(1);

    // TODO: Support multiple CPUs with different lapic ids (mapping)
    for lapic in lapic_ids {
        log!("init lapic {lapic} @ {lapic_base:#x}");
        LAPIC.init(lapic, NonZeroUsize::new(lapic_base as _));
    }

    log!("init ioapic {ioapic_id} @ {ioapic_base:x?}");
    IOAPIC.init(ioapic_base, ioapic_id, Vector::Panic as u8 as _);
    IOAPIC.config(slot_map[Device::Keyboard as usize], Vector::Keyboard as u8, true);
    IOAPIC.allow(slot_map[Device::Keyboard as usize]);

    // Initialize the timer for the scheduler
    LAPIC.timer(10000, Vector::Timer as _, true, false);
}

fn eoi(_vector: u8) {
    LAPIC.eoi()
}

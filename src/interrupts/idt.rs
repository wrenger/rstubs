use core::arch::asm;

use crate::arch::int::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};

use crate::arch::registers::Cr2;
use crate::keyboard::KEYBOARD;

use super::gate::Gate;
use super::guard::Guard;
use super::Vector;

static mut IDT: InterruptDescriptorTable = InterruptDescriptorTable::new();

/// Init the interrupt descriptor table.
pub fn init() {
    unsafe {
        IDT.double_fault.set_handler_fn(double_fault);
        IDT.invalid_tss.set_handler_fn(invalid_tss);
        IDT.general_protection_fault
            .set_handler_fn(general_protection_fault);
        IDT.page_fault.set_handler_fn(page_fault);
        IDT[Vector::Timer as _].set_handler_fn(timer);
        IDT[Vector::Keyboard as _].set_handler_fn(keyboard);
        IDT[Vector::Panic as _].set_handler_fn(panic);
        IDT.load();
    }
}

/// ISR for double faults.
///
/// A double fault is a special exception that occurs when the CPU fails to
/// invoke an exception handler.
/// By handling this we prevent a system reset.
extern "x86-interrupt" fn double_fault(stack: &mut InterruptStackFrame, error: u32) -> ! {
    panic!("DOUBLE FAULT {:x}\n{:#?}", error, stack);
}

/// Invalid TSS register
extern "x86-interrupt" fn invalid_tss(stack: &mut InterruptStackFrame, error: u32) {
    panic!("INVALID TSS {:x}\n{:#?}", error, stack);
}

/// Invalid memory accesses with segmentation
extern "x86-interrupt" fn general_protection_fault(stack: &mut InterruptStackFrame, error: u32) {
    panic!("GENERAL PROTECTION FAULT {:x}\n{:#?}", error, stack);
}

/// Invalid memory accesses with paging
extern "x86-interrupt" fn page_fault(stack: &mut InterruptStackFrame, error: PageFaultErrorCode) {
    // The accessed address is stored in the cr2 register
    panic!("PAGE FAULT {:?} at {:#x}\n{:#?}", error, Cr2::read(), stack);
}

extern "x86-interrupt" fn panic(stack: &mut InterruptStackFrame) {
    panic!("PANIC\n{:#?}", stack);
}

extern "x86-interrupt" fn timer(_stack_frame: &mut InterruptStackFrame) {
    super::eoi(Vector::Timer as _);
    Guard::relay(Gate::Timer);
}

extern "x86-interrupt" fn keyboard(_stack_frame: &mut InterruptStackFrame) {
    let keyboard = unsafe { &mut KEYBOARD };
    if let Some(key) = keyboard.key_hit() {
        super::eoi(Vector::Keyboard as _);
        Guard::relay(Gate::Keyboard { key });
    } else {
        super::eoi(Vector::Keyboard as _);
    }
}

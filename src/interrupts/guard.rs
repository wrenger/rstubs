//! Implementation of the prologue/epilogue modell.

use super::gate::{Epilog, Gate};
use crate::arch;

// static LOCK: spin::Mutex<()> = spin::Mutex::new(());
static mut LOCK: usize = 1;

/// The guard protects and synchronizes the epilogue layer.
#[derive(Debug)]
pub struct Guard<'a> {
    _t: core::marker::PhantomData<&'a ()>,
}

impl<'a> Guard<'a> {
    /// Returns if the layer epilogue is occupied.
    pub fn is_locked() -> bool {
        unsafe { LOCK != 0 }
    }

    fn lock() {
        unsafe { LOCK += 1 };
    }

    fn unlock() {
        unsafe { LOCK -= 1 };
    }

    /// Enter the epilogue layer or wait synchronously if it is already occupied.
    ///
    /// A guard object is returned that unlocks the epilogue layer when it falls out of scope.
    pub fn enter() -> Guard<'a> {
        Guard::lock();
        Guard {
            _t: core::marker::PhantomData,
        }
    }

    fn try_enter() -> Option<Guard<'a>> {
        if !Guard::is_locked() {
            Some(Guard::enter())
        } else {
            None
        }
    }

    /// Register the given epilogue, which is either executed directly if possible
    /// or it is enqueued for later execution.
    pub fn relay(mut gate: Gate) {
        if let Some(_guard) = Guard::try_enter() {
            arch::int::enable();
            gate.epilog();
        } else if !Epilog::push(gate) {
            dprint!("!");
        }
    }

    /// Leave the epilogue layer.
    pub fn leave() {
        arch::int::disable();
        while let Some(mut gate) = Epilog::pop() {
            arch::int::enable();
            gate.epilog(); // <- resume: Thread switch!!!
            arch::int::disable();
        }

        Guard::unlock();
        arch::int::enable();
    }
}

impl<'a> Drop for Guard<'a> {
    fn drop(&mut self) {
        Guard::leave();
    }
}

use core::ops::Range;
use core::ptr::null_mut;

use spin::mutex::TicketMutex;

use crate::arch::paging::Page;

/// Simple free-list based page allocator.
pub struct Alloc {
    memory: Range<*const Page>,
    next: TicketMutex<Node>,
}

pub static mut ALLOC: Alloc = Alloc::new();

impl Alloc {
    pub const fn new() -> Self {
        Self {
            memory: null_mut()..null_mut(),
            next: TicketMutex::new(Node::new()),
        }
    }

    #[cold]
    pub fn init(&mut self, memory: &mut [Page]) {
        log!("alloc init {:?} {}", memory.as_ptr_range(), memory.len());

        let begin = memory.as_ptr() as usize;
        let pages = memory.len();

        // build free lists
        for i in 1..pages {
            unsafe {
                memory[i - 1]
                    .cast_mut::<Node>()
                    .set((begin + i * Page::SIZE) as *mut _);
            }
        }
        unsafe { memory[pages - 1].cast_mut::<Node>().set(null_mut()) };

        self.memory = memory.as_ptr_range();
        self.next = TicketMutex::new(Node(begin as _));
    }

    #[inline(never)]
    pub fn get(&self) -> Result<usize, ()> {
        if let Some(node) = self.next.lock().pop() {
            let addr = node as *mut _ as usize;
            debug_assert!(addr % Page::SIZE == 0 && self.memory.contains(&(addr as _)));
            Ok(addr)
        } else {
            log!("No memory");
            Err(())
        }
    }

    #[inline(never)]
    pub fn put(&self, addr: usize) -> Result<(), ()> {
        if addr % Page::SIZE != 0 || !self.memory.contains(&(addr as _)) {
            log!("invalid addr");
            return Err(());
        }

        self.next.lock().push(unsafe { &mut *(addr as *mut Node) });
        Ok(())
    }
}

struct Node(*mut Node);

impl Node {
    const fn new() -> Self {
        Self(null_mut())
    }
    fn set(&mut self, v: *mut Node) {
        self.0 = v;
    }
    fn push(&mut self, v: &mut Node) {
        v.0 = self.0;
        self.0 = v;
    }
    fn pop(&mut self) -> Option<&mut Node> {
        if !self.0.is_null() {
            let curr = unsafe { &mut *self.0 };
            self.0 = curr.0;
            Some(curr)
        } else {
            None
        }
    }
}

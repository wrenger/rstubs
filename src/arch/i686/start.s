# Functions that have to be implemented in rust
.extern setup_paging # fn() -> *const PageDirectory
.extern setup_gdt # fn() -> *const DiscriptorTablePointer
.extern kmain # fn()

# === Multiboot Header ===
MULTIBOOT_PAGE_ALIGN  =  (1<<0)
MULTIBOOT_MEMORY_INFO =  (1<<1)
# MULTIBOOT_REQVIDMODE  =  (1<<2)
MULTIBOOT_HEADER_MAGIC =  0x1BADB002
MULTIBOOT_HEADER_FLAGS = (MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO)
MULTIBOOT_CHECKSUM     = -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)
.section .multiboot, "a"
.globl mboot
mboot:
	.long MULTIBOOT_HEADER_MAGIC
	.long MULTIBOOT_HEADER_FLAGS
	.long MULTIBOOT_CHECKSUM
	.long mboot
	# a.out kludge (not used, the kernel is elf)
	.long 0, 0, 0, 0	# load_addr, load_end_addr, bss_end_addr, entry_addr
	# Video mode
	.long 0 	# Mode type (0: LFB)
	.long 0 	# Width (no preference)
	.long 0 	# Height (no preference)
	.long 32	# Depth (32-bit preferred)

# === Code ===
.section .inittext, "ax"
.globl start
start:
	# Save multiboot state
	mov MBOOT_SIG, eax
	mov MBOOT_PTR, ebx

	# Prepare the gdt
	mov esp, offset init_stack
	call setup_gdt
	# Returns the ptr to the descriptor table ptr in eax

	lgdt [eax]

	# Jump High and set CS
	# Well I did not find the correct intel syntax for this...
	# `jmp 0x8:start_high` or `ljmp 0x8, start_high` do not work.
.att_syntax
   	ljmp $0x8, $start_high
.intel_syntax


.section .text
.globl start_high
start_high:
	# Prep segment registers
	mov ax, 0x10
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov esp, offset init_stack
	call kmain

	# If kmain returns, loop forefer
.l:
	hlt
	jmp .l

# === Read-write data ===
.section .data
.globl MBOOT_SIG
MBOOT_SIG:
	.long 0
.globl MBOOT_PTR
MBOOT_PTR:
	.long 0

.section .bss
.align 0x1000
	.space 0x1000
init_stack:

use core::arch::asm;
use core::fmt;
use core::mem::size_of;

use crate::arch::segmentation::SegmentSelector;
use crate::logging::Hex;

/// Task state segment register, that holds information about task-switching.
#[repr(C, packed)]
#[allow(non_snake_case)]
pub struct TaskStateSegment {
    reserved_00: u32,
    pub esp0: u32,
    pub ss0: u16,
    reserved_0A: u16,
    reserved_0C_60: [u32; 24],
}

const _: () = assert!(size_of::<TaskStateSegment>() == 0x6c);

impl fmt::Debug for TaskStateSegment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("TaskStateSegment")
            .field("esp0", &Hex(self.esp0))
            .field("ss0", &Hex(self.ss0))
            .finish()
    }
}

impl TaskStateSegment {
    pub const fn new(esp0: u32, ss0: u16) -> TaskStateSegment {
        TaskStateSegment {
            reserved_00: 0,
            esp0,
            ss0,
            reserved_0A: 0,
            reserved_0C_60: [0; 24],
        }
    }
}

#[inline]
pub unsafe fn load_tss(sel: SegmentSelector) {
    asm!("ltr {0:x}", in(reg) sel.0, options(nostack, nomem));
}

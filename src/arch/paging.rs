use bitfield_struct::bitfield;
use core::alloc::Layout;
use core::arch::asm;
use core::mem::{size_of, transmute};

pub fn set_pd(pd: &PageDirectory) {
    unsafe { asm!("mov cr3, {0}", in(reg) pd, options(nostack, nomem)) };
}

pub fn enable() {
    unsafe {
        asm!(
            "mov eax, cr0
            or eax, 0x80010000
            mov cr0, eax",
            out("eax") _,
            options(nostack, nomem),
        )
    };
}

#[derive(Clone)]
#[repr(align(0x1000))]
pub struct Page {
    _data: [u8; Page::SIZE],
}

const _: () = assert!(Layout::new::<Page>().size() == Page::SIZE);
const _: () = assert!(Layout::new::<Page>().align() == Page::SIZE);

impl Page {
    pub const SIZE_BITS: usize = 12; // 2^12 => 4KiB
    pub const SIZE: usize = 1 << Page::SIZE_BITS;
    pub const fn new() -> Self {
        Self {
            _data: [0; Page::SIZE],
        }
    }
    pub unsafe fn cast<T>(&self) -> &T {
        debug_assert!(size_of::<T>() <= size_of::<Self>());
        transmute(self)
    }
    pub unsafe fn cast_mut<T>(&mut self) -> &mut T {
        debug_assert!(size_of::<T>() <= size_of::<Self>());
        transmute(self)
    }
}

/// Return the smalles aligned number larger than `v`.
pub const fn align_up(v: usize, align: usize) -> usize {
    (v + align - 1) & !(align - 1)
}

/// Return the largest aligned number less than `v`.
pub const fn align_down(v: usize, align: usize) -> usize {
    v & !(align - 1)
}

pub const ENTRIES: usize = Page::SIZE / size_of::<usize>();

#[bitfield(u32)]
#[derive(Default)]
pub struct Entry {
    present: bool,
    write: bool,
    user: bool,
    #[bits(9)]
    _p: u32,
    #[bits(20)]
    page: usize,
}
const _: () = assert!(size_of::<Entry>() == size_of::<usize>());

impl Entry {
    pub const fn kernel(addr: usize) -> Entry {
        Entry::new()
            .with_present(true)
            .with_write(true)
            .with_page(addr >> 12)
    }

    #[inline]
    pub const fn addr<T>(self) -> *mut T {
        debug_assert!(size_of::<T>() <= Page::SIZE);

        if self.present() {
            (self.page() << 12) as *mut T
        } else {
            core::ptr::null_mut()
        }
    }
}

#[repr(C)]
#[repr(align(0x1000))]
pub struct PageTable {
    pub entries: [Entry; ENTRIES],
}

const _: () = assert!(size_of::<PageTable>() == Page::SIZE);

impl PageTable {
    pub const fn new() -> Self {
        Self {
            entries: [Entry::new(); ENTRIES],
        }
    }
}

#[repr(C)]
#[repr(align(0x1000))]
pub struct PageDirectory {
    pub entries: [Entry; ENTRIES],
}

impl PageDirectory {
    pub const fn new() -> PageDirectory {
        PageDirectory {
            entries: [Entry::new(); ENTRIES],
        }
    }

    /// Create a mapping `from` a virtual address `to` a physical page.
    pub fn map(&mut self, from: usize, to: usize) -> Result<(), ()> {
        let idx = PageIndex::ptr(from);
        if let Some(table) = self.table_mut(idx.pdi as usize) {
            table.entries[idx.pti as usize] = Entry::kernel(to);
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn get(&self, idx: PageIndex) -> Entry {
        if !idx.valid() {
            return Entry::new();
        }
        if let Some(table) = self.table(idx.pdi as usize) {
            table.entries[idx.pti as usize]
        } else {
            Entry::new()
        }
    }

    pub fn table(&self, at: usize) -> Option<&PageTable> {
        if at >= ENTRIES {
            return None;
        }
        let entry = self.entries[at];
        unsafe { entry.addr::<PageTable>().as_ref() }
    }

    pub fn table_mut(&mut self, at: usize) -> Option<&mut PageTable> {
        if at >= ENTRIES {
            return None;
        }
        let entry = self.entries[at];
        unsafe { entry.addr::<PageTable>().as_mut() }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(packed)]
pub struct PageIndex {
    pub pdi: u16,
    pub pti: u16,
}

impl PageIndex {
    pub const fn new(pdi: u16, pti: u16) -> PageIndex {
        PageIndex { pdi, pti }
    }

    pub const fn ptr(ptr: usize) -> PageIndex {
        let ptr = ptr / Page::SIZE;
        PageIndex {
            pdi: (ptr / ENTRIES) as u16,
            pti: (ptr % ENTRIES) as u16,
        }
    }

    pub const fn valid(self) -> bool {
        self.pdi < ENTRIES as u16 && self.pti < ENTRIES as u16
    }
}

use core::arch::asm;

bitflags::bitflags! {
    /// The RFLAGS register.
    #[derive(Default)]
    pub struct Flags: usize {
        /// Set by hardware if last arithmetic operation generated a carry out of the
        /// most-significant bit of the result.
        const CARRY_FLAG = 1;
        /// Set by hardware if last result has an even number of 1 bits (only for some operations).
        const PARITY_FLAG = 1 << 2;
        /// Set by hardware if last arithmetic operation generated a carry ouf of bit 3 of the
        /// result.
        const AUXILIARY_CARRY_FLAG = 1 << 4;
        /// Set by hardware if last arithmetic operation resulted in a zero value.
        const ZERO_FLAG = 1 << 6;
        /// Set by hardware if last arithmetic operation resulted in a negative value.
        const SIGN_FLAG = 1 << 7;
        /// Enable single-step mode for debugging.
        const TRAP_FLAG = 1 << 8;
        /// Enable interrupts.
        const INTERRUPT_FLAG = 1 << 9;
        /// Determines the order in which strings are processed.
        const DIRECTION_FLAG = 1 << 10;
        /// Set by hardware to indicate that the sign bit of the result of the last signed integer
        /// operation differs from the source operands.
        const OVERFLOW_FLAG = 1 << 11;
        /// Specifies the privilege level required for executing I/O address-space instructions.
        const IOPL_LOW = 1 << 12;
        /// Specifies the privilege level required for executing I/O address-space instructions.
        const IOPL_HIGH = 1 << 13;
        /// Used by `iret` in hardware task switch mode to determine if current task is nested.
        const NESTED_TASK = 1 << 14;
        /// Temporarily disables debug exceptions so that an instruction can be restarted after a debug exception
        /// without immediately causing another debug exception.
        const RESUME_FLAG = 1 << 16;
        /// Enable the virtual-8086 mode.
        const VIRTUAL_8086_MODE = 1 << 17;
        /// Enable automatic alignment checking if CR0.AM is set. Only works if CPL is 3.
        const ALIGNMENT_CHECK = 1 << 18;
        /// Virtual interrupt flag.
        const VIRTUAL_INTERRUPT = 1 << 19;
        /// Virtual interrupt pending.
        const VIRTUAL_INTERRUPT_PENDING = 1 << 20;
        /// Able to use CPUID instruction.
        const ID = 1 << 21;
    }
}

impl Flags {
    #[inline]
    pub fn read() -> Flags {
        Flags::from_bits_truncate(Flags::read_raw())
    }

    #[inline]
    pub unsafe fn write(self) {
        let old = Flags::read_raw();
        let reserved = old & !Flags::all().bits();
        Flags::write_raw(reserved | self.bits());
    }

    #[inline]
    fn read_raw() -> usize {
        let value: usize;
        unsafe {
            asm!("mov eax, cr0", out("eax") value, options(nomem));
        }
        value
    }

    #[inline]
    unsafe fn write_raw(value: usize) {
        asm!("mov cr0, {}", in(reg) value, options(nostack));
    }
}

bitflags::bitflags! {
    /// Configuration flags of the Cr0 register.
    #[derive(Default)]
    pub struct Cr0: usize {
        /// Enables protected mode.
        const PROTECTED_MODE_ENABLE = 1;
        /// Enables monitoring of the coprocessor, typical for x87 instructions.
        ///
        /// Controls together with the `TASK_SWITCHED` flag whether a `wait` or `fwait`
        /// instruction should cause a device-not-available exception.
        const MONITOR_COPROCESSOR = 1 << 1;
        /// Force all x87 and MMX instructions to cause an exception.
        const EMULATE_COPROCESSOR = 1 << 2;
        /// Automatically set to 1 on _hardware_ task switch.
        ///
        /// This flags allows lazily saving x87/MMX/SSE instructions on hardware context switches.
        const TASK_SWITCHED = 1 << 3;
        /// Enables the native error reporting mechanism for x87 FPU errors.
        const NUMERIC_ERROR = 1 << 5;
        /// Controls whether supervisor-level writes to read-only pages are inhibited.
        const WRITE_PROTECT = 1 << 16;
        /// Enables automatic alignment checking.
        const ALIGNMENT_MASK = 1 << 18;
        /// Ignored. Used to control write-back/write-through cache strategy on older CPUs.
        const NOT_WRITE_THROUGH = 1 << 29;
        /// Disables internal caches (only for some cases).
        const CACHE_DISABLE = 1 << 30;
        /// Enables page translation.
        const PAGING = 1 << 31;
    }
}

impl Cr0 {
    #[inline]
    pub fn read() -> Cr0 {
        Cr0::from_bits_truncate(Cr0::read_raw())
    }

    #[inline]
    pub unsafe fn write(self) {
        let old = Cr0::read_raw();
        let reserved = old & !Cr0::all().bits();
        Cr0::write_raw(reserved | self.bits());
    }

    #[inline]
    fn read_raw() -> usize {
        let value: usize;
        unsafe {
            asm!("mov {}, cr0", out(reg) value, options(nomem));
        }
        value
    }

    #[inline]
    unsafe fn write_raw(value: usize) {
        asm!("mov cr0, {}", in(reg) value, options(nostack));
    }
}

pub struct Cr2;

impl Cr2 {
    #[inline]
    pub fn read() -> usize {
        let value: usize;
        unsafe {
            asm!("mov {}, cr2", out(reg) value, options(nomem));
        }
        value
    }
}

pub struct Cr3;

impl Cr3 {
    #[inline]
    pub fn read() -> usize {
        let value: usize;
        unsafe {
            asm!("mov {}, cr3", out(reg) value, options(nomem));
        }
        value
    }

    #[inline]
    pub unsafe fn write(value: usize) {
        asm!("mov cr3, {}", in(reg) value, options(nostack));
    }
}

#[derive(Debug)]
#[repr(transparent)]
pub struct Msr(pub u32);

impl Msr {
    pub unsafe fn read(self) -> u64 {
        let (high, low): (u32, u32);
        asm!("rdmsr", out("eax") low, out("edx") high, in("ecx") self.0, options(nostack));
        ((high as u64) << 32) | (low as u64)
    }

    pub unsafe fn write(self, value: u64) {
        let low = value as u32;
        let high = (value >> 32) as u32;
        asm!("wrmsr", in("ecx") self.0, in("eax") low, in("edx") high, options(nostack))
    }
}

//! The old/historical PIT "Programmable Interval Timer (PIT)"

use core::{hint, mem::transmute};

use bitfield_struct::bitfield;

use super::io::{Port, PortValue};

/// We only use PIT channel 2
///
/// Channel 2 is not able to send interrupts
const CHANNEL: u8 = 2;
const MODE: Port<Mode> = Port::new(0x43);
const CTRL: Port<Ctrl> = Port::new(0x61);
const DATA: Port<u8> = Port::new(0x40 + CHANNEL as u16);
const BASE_FREQUENCY: u64 = 1193182;

/// The old/historical PIT "Programmable Interval Timer (PIT)"
///
/// Historically, PCs had a Timer component of type 8253 or 8254,
/// modern systems come with a compatible chip.
/// Each of these chips provides three 16-bit wide counters ("channel"),
/// each running at a frequency of 1.19318 MHz.
/// The timer's counting speed is thereby independent from the CPU frequency.
///
/// Traditionally, the first counter (channel 0) was used for triggering interrupts,
/// the second one (channel 1) controlled
/// the memory refresh, and the third counter (channel 2) was assigned to the PC speaker.
///
/// As the PIT's frequency is fixed to a constant value of 1.19318 MHz,
/// the PIT can be used for calibration.
/// For this purpose, we use channel 2 only.
pub struct Timer;

impl Timer {
    /// Initialize the timer for `us` micro seconds.
    pub fn new(us: u16) -> Option<Self> {
        let counter = BASE_FREQUENCY * us as u64 / 1000000;

        if counter > 0xffff {
            return None;
        }

        unsafe {
            CTRL.update(|v: Ctrl| {
                v.with_enable_speaker_data(false)
                    .with_enable_timer_counter2(true)
            });

            MODE.write(
                Mode::new()
                    .with_access(Access::LowAndHighByte)
                    .with_operating(Operating::InterruptOnTerminalCount)
                    .with_format(false)
                    .with_channel(CHANNEL)
                    .into(),
            );

            DATA.write(counter as u8);
            DATA.write((counter >> 8) as u8);
        }

        Some(Self)
    }

    /// Actively wait for the timer to finish.
    pub fn wait(self) -> bool {
        loop {
            let ctrl = unsafe { CTRL.read() };
            if !ctrl.enable_timer_counter2() {
                return false;
            } else if ctrl.status_timer_counter2() {
                return true;
            } else {
                hint::spin_loop()
            }
        }
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        // Disable
        unsafe {
            CTRL.update(|c| {
                c.with_enable_speaker_data(false)
                    .with_enable_timer_counter2(false)
            })
        }
    }
}

/// Mode register (only writable)
#[bitfield(u8)]
struct Mode {
    format: bool,
    #[bits(3)]
    operating: Operating,
    #[bits(2)]
    access: Access,
    #[bits(2)]
    channel: u8,
}
impl PortValue for Mode {
    type I = u8;
}

/// Operating mode
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
enum Operating {
    InterruptOnTerminalCount = 0,
    ProgrammableOneShot = 1,
    RateGenerator = 2,
    /// useful for the PC speaker
    SquareWaveGenerator = 3,
    SoftwareTriggeredStrobe = 4,
    HardwareTriggeredStrobe = 5,
}
impl From<u8> for Operating {
    fn from(value: u8) -> Self {
        unsafe { transmute(value) }
    }
}
impl From<Operating> for u8 {
    fn from(value: Operating) -> Self {
        value as _
    }
}

/// Access mode
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
enum Access {
    LatchCountValue = 0,
    LowByteOnly = 1,
    HighByteOnly = 2,
    LowAndHighByte = 3,
}
impl From<u8> for Access {
    fn from(value: u8) -> Self {
        unsafe { transmute(value) }
    }
}
impl From<Access> for u8 {
    fn from(value: Access) -> Self {
        value as _
    }
}

/// Timer control register
#[bitfield(u8)]
struct Ctrl {
    enable_timer_counter2: bool,
    /// If set, speaker output is equal to status_timer_counter2
    enable_speaker_data: bool,
    /// Not important, do not modify
    enable_pci_serr: bool,
    /// Not important, do not modify
    enable_nmi_iochk: bool,
    /// Not important, must be 0 on write
    refresh_cycle_toggle: bool,
    /// Will be set on timer expiration; must be 0 on write
    status_timer_counter2: bool,
    /// Not important, must be 0 on write
    status_iochk_nmi_source: bool,
    /// Not important, must be 0 on write
    status_serr_nmi_source: bool,
}
impl PortValue for Ctrl {
    type I = u8;
}

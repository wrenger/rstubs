use core::arch::asm;
use core::fmt;
use core::mem::size_of;

use crate::arch::segmentation::SegmentSelector;
use crate::arch::tss::TaskStateSegment;
use crate::logging::Hex;

/// A struct describing a pointer to a descriptor table (GDT / IDT).
/// This is in a format suitable for giving to 'lgdt' or 'lidt'.
#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct DescriptorTablePointer {
    /// Size of the DT.
    pub limit: u16,
    /// Pointer to the memory region containing the DT.
    pub base: *mut Descriptor,
}

mod flags {
    pub const DPL_RING_3: u64 = 3 << 45;
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Descriptor(u64);

impl Descriptor {
    /// Creates a new raw descriptor with the given value.
    pub const fn null() -> Descriptor {
        Descriptor(0)
    }

    pub const fn kernel_code() -> Descriptor {
        // TODO: Split up into flags
        Descriptor(0x00CF_9A00_0000_FFFF)
    }

    pub const fn kernel_data() -> Descriptor {
        // TODO: Split up into flags
        Descriptor(0x00CF_9200_0000_FFFF)
    }

    pub const fn user_code() -> Descriptor {
        // TODO: Split up into flags
        Descriptor(0x00CF_FA00_0000_FFFF)
    }

    pub const fn user_data() -> Descriptor {
        // TODO: Split up into flags
        Descriptor(0x00CF_F200_0000_FFFF)
    }

    /// Creates a new task state segment descriptor for the Global Descriptor Table.
    pub fn tss(tss: &TaskStateSegment) -> Descriptor {
        let base = tss as *const _ as u64;
        let limit = (size_of::<TaskStateSegment>() - 1) as u64;

        let mut value = 0;
        value |= limit & 0x0000ffff;
        value |= base & 0x00ffffff << 16;
        value |= base & 0xff000000 << 32;
        value |= 0b1001 << 40; // sys_type: 80386-TSS, avaliable
        Descriptor(value)
    }
}

impl fmt::Debug for Descriptor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("Descriptor").field(&Hex(self.0)).finish()
    }
}

pub unsafe fn set(
    ptr: &DescriptorTablePointer,
    index: u16,
    descriptor: Descriptor,
) -> SegmentSelector {
    assert!(
        ptr.limit as usize >= index as usize * size_of::<Descriptor>(),
        "index out of range"
    );
    *ptr.base.add(index as _) = descriptor;
    lgdt(ptr);

    SegmentSelector::new(index, descriptor.0 & flags::DPL_RING_3 == flags::DPL_RING_3)
}

#[inline]
pub unsafe fn lgdt(ptr: &DescriptorTablePointer) {
    asm!("lgdt [{}]", in(reg) ptr, options(nostack));
}

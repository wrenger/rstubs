use core::slice;

use multiboot::information::{MemoryManagement, Multiboot, PAddr};

static mut MB_MEM: MultibootMem = MultibootMem;

struct MultibootMem;
impl MemoryManagement for MultibootMem {
    unsafe fn paddr_to_slice(&self, addr: PAddr, length: usize) -> Option<&'static [u8]> {
        Some(slice::from_raw_parts(addr as usize as *const _, length))
    }

    unsafe fn allocate(&mut self, length: usize) -> Option<(PAddr, &mut [u8])> {
        log!("multiboot alloc! {length}");
        None
    }

    unsafe fn deallocate(&mut self, _addr: PAddr) {}
}

pub fn load(signature: u32, ptr: *const ()) -> Multiboot<'static, 'static> {
    assert_eq!(signature, multiboot::information::SIGNATURE_EAX);
    unsafe { Multiboot::from_ptr(ptr as usize as u64, &mut MB_MEM) }.unwrap()
}

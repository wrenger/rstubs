use core::marker::PhantomData;
use core::mem::{self, size_of, transmute};

use core::ops::Deref;
use core::{ops::Range, slice, str};

use bitfield_struct::bitfield;

pub fn init() -> Result<Acpi, ApicError> {
    let rsdp = unsafe { Rsdp::search_for_on_bios() }?;
    log!(
        "found rsdp oem='{}' @ {:?}",
        rsdp.oem_id(),
        rsdp as *const _
    );

    let acpi = if rsdp.revision != 0 && rsdp.length >= 36 {
        // If the XSDT is present we must use it; see:
        // ACPI Specification Revision 4.0a:
        // "An ACPI-compatible OS must use the XSDT if present."
        let table = unsafe { &*(rsdp.xsdt_address as *const SysDescTable) };
        log!("xsdt 0x{:?} valid={}", table as *const _, table.valid());
        Acpi::new(table, 8)
    } else {
        let table = unsafe { &*(rsdp.rsdt_address as *const SysDescTable) };
        log!("rsdt 0x{:?} valid={}", table as *const _, table.valid());
        Acpi::new(table, 4)
    };
    if acpi.root.valid() {
        for i in 0..acpi.len() {
            if let Some(entry) = acpi.entry(i) {
                log!("{i}: {}", entry.signature());
            }
        }
        Ok(acpi)
    } else {
        Err(ApicError::InvalidChecksum)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Acpi {
    root: &'static SysDescTable,
    entry_size: usize,
}

impl Acpi {
    fn new(root: &'static SysDescTable, entry_size: usize) -> Self {
        Self { root, entry_size }
    }

    pub fn len(&self) -> usize {
        (self.root.length as usize - size_of::<SysDescTable>()) / self.entry_size
    }

    pub fn entry(&self, i: usize) -> Option<&'static SysDescTable> {
        if i >= self.len() {
            return None;
        }
        let offset = size_of::<SysDescTable>() + i * self.entry_size;
        let entry_ptr = unsafe { (self.root as *const _ as *const u8).add(offset) };
        let entry = unsafe { *entry_ptr.cast::<usize>() };
        let table = unsafe { &*(entry as *const SysDescTable) };
        if table.valid() {
            Some(table)
        } else {
            log!("Invalid table: {i}");
            None
        }
    }

    pub fn find(&self, signature: [u8; 4]) -> Option<&'static SysDescTable> {
        for i in 0..self.len() {
            let entry_ptr = unsafe {
                (self.root as *const _ as *const u8)
                    .add(size_of::<SysDescTable>() + i * self.entry_size)
            };
            let entry = unsafe { *entry_ptr.cast::<usize>() };
            let table = unsafe { &*(entry as *const SysDescTable) };
            if table.signature == signature {
                if table.valid() {
                    return Some(table);
                } else {
                    log!("Invalid table: {i} {signature:?}");
                }
            }
        }
        None
    }
}

/// Simple marker trait for the other ACPI table types
pub trait AcpiTable {}

/// Header of an ACPI table
#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct SysDescTable {
    signature: [u8; 4],
    length: u32,
    revision: u8,
    checksum: u8,
    oemid: [u8; 6],
    oem_table_id: [u8; 8],
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32,
}

const _: () = assert!(size_of::<SysDescTable>() == 36);

impl SysDescTable {
    pub fn valid(&self) -> bool {
        let mem = unsafe { slice::from_raw_parts(self as *const _ as *const u8, self.length as _) };
        let mut sum = 0u8;
        for &b in mem {
            sum = sum.wrapping_add(b);
        }
        sum == 0
    }

    pub fn signature(&self) -> &str {
        unsafe { str::from_utf8_unchecked(&self.signature) }
    }

    pub fn cast<T: AcpiTable>(&self) -> &T {
        unsafe { mem::transmute(self) }
    }
}

/// Describes the different LAPICs
#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct MulAcpiTable {
    pub header: SysDescTable,
    pub lapic_address: u32,
    pub flags: MulAcpiTableF,
}

#[bitfield(u32)]
pub struct MulAcpiTableF {
    pub pcat_compat: bool,
    #[bits(31)]
    _p: u32,
}

impl MulAcpiTable {
    pub fn iter(&self) -> MulAcpiTableIter {
        let start = self as *const _ as usize;
        MulAcpiTableIter {
            start: start + size_of::<Self>(),
            end: start + self.header.length as usize,
            _phantom: PhantomData,
        }
    }
}

pub struct MulAcpiTableIter<'a> {
    start: usize,
    end: usize,
    _phantom: PhantomData<&'a ()>,
}

impl<'a> Iterator for MulAcpiTableIter<'a> {
    type Item = &'a ApicEntry;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            let entry = unsafe { &*(self.start as *const ApicEntry) };
            self.start += entry.len();
            Some(entry)
        } else {
            None
        }
    }
}

#[derive(Debug)]
#[repr(u8, C)]
#[non_exhaustive]
pub enum ApicEntry {
    LAPIC(ApicEntryLApic) = 0,
    IOAPIC(ApicEntryIOApic) = 1,
    IntSrcOverride(ApicEntryIntSrcOverride) = 2,
    _3 = 3,
    _4 = 4,
    LAPICAddrOverride(ApicEntryLAPICAddrOverride) = 5,
    _6 = 6,
    _7 = 7,
    _8 = 8,
    _9 = 9,
}

impl ApicEntry {
    fn len(&self) -> usize {
        #[repr(C, packed)]
        struct Header {
            ty: u8,
            len: u8,
        }
        let h: &Header = unsafe { transmute(self) };
        h.len as _
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct ApicEntryLApic {
    pub len: u8,
    pub apic_cid: u8,
    pub apic_id: u8,
    pub flags: ApicEntryLApicF,
}

#[bitfield(u32)]
pub struct ApicEntryLApicF {
    pub enabled: bool,
    #[bits(31)]
    _p: u32,
}

#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct ApicEntryIOApic {
    pub len: u8,
    pub ioapic_id: u8,
    pub _reserved: u8,
    pub ioapic_addr: u32,
    pub global_int_base: u32,
}

#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct ApicEntryIntSrcOverride {
    pub len: u8,
    pub bus: u8,
    pub source: u8,
    pub global_int: u32,
    pub flags: u16,
}

#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
pub struct ApicEntryLAPICAddrOverride {
    pub len: u8,
    pub _reserved: u16,
    pub lapic_addr: u64,
}

impl Deref for MulAcpiTable {
    type Target = SysDescTable;
    fn deref(&self) -> &Self::Target {
        &self.header
    }
}

impl AcpiTable for MulAcpiTable {}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum ApicError {
    NoValidRsdp,
    IncorrectSignature,
    InvalidOemId,
    InvalidChecksum,
}

/// The first structure found in ACPI. It just tells us where the RSDT is.
///
/// On BIOS systems, it is either found in the first 1KB of the Extended Bios Data Area, or between
/// 0x000E0000 and 0x000FFFFF. The signature is always on a 16 byte boundary. On (U)EFI, it may not
/// be located in these locations, and so an address should be found in the EFI configuration table
/// instead.
///
/// The recommended way of locating the RSDP is to let the bootloader do it - Multiboot2 can pass a
/// tag with the physical address of it. If this is not possible, a manual scan can be done.
///
/// If `revision > 0`, (the hardware ACPI version is Version 2.0 or greater), the RSDP contains
/// some new fields. For ACPI Version 1.0, these fields are not valid and should not be accessed.
/// For ACPI Version 2.0+, `xsdt_address` should be used (truncated to `u32` on x86) instead of
/// `rsdt_address`.
#[derive(Clone, Copy, Debug)]
#[repr(C, packed)]
struct Rsdp {
    signature: [u8; 8],
    checksum: u8,
    oem_id: [u8; 6],
    revision: u8,
    rsdt_address: u32,

    /*
     * These fields are only valid for ACPI Version 2.0 and greater
     */
    length: u32,
    xsdt_address: u64,
    ext_checksum: u8,
    reserved: [u8; 3],
}

impl Rsdp {
    /// This searches for a RSDP on BIOS systems.
    unsafe fn search_for_on_bios() -> Result<&'static Rsdp, ApicError> {
        for area in find_search_areas() {
            for address in area.step_by(16) {
                let rsdp = unsafe { &*(address as *const Rsdp) };
                if rsdp.signature == *RSDP_SIGNATURE {
                    if let Err(err) = rsdp.validate() {
                        log!("Invalid RSDP found at {address:#x}: {err:?}")
                    } else {
                        return Ok(rsdp);
                    }
                }
            }
        }
        Err(ApicError::NoValidRsdp)
    }

    /// Checks that:
    ///     1) The signature is correct
    ///     2) The checksum is correct
    ///     3) For Version 2.0+, that the extension checksum is correct
    fn validate(&self) -> Result<(), ApicError> {
        const RSDP_V1_LENGTH: usize = 20;

        // Check the signature
        if &self.signature != RSDP_SIGNATURE {
            return Err(ApicError::IncorrectSignature);
        }
        // Check the OEM id is valid UTF8 (allows use of unwrap)
        if str::from_utf8(&self.oem_id).is_err() {
            return Err(ApicError::InvalidOemId);
        }
        // `self.length` doesn't exist on ACPI version 1.0, so we mustn't rely on it.
        // Instead, check for version 1.0 and use a hard-coded length.
        let length = if self.revision > 0 {
            self.length as usize
        } else {
            RSDP_V1_LENGTH
        };

        let bytes = unsafe { slice::from_raw_parts(self as *const Rsdp as *const u8, length) };
        let sum = bytes.iter().fold(0u8, |sum, &byte| sum.wrapping_add(byte));
        if sum != 0 {
            Err(ApicError::InvalidChecksum)
        } else {
            Ok(())
        }
    }

    fn oem_id(&self) -> &str {
        str::from_utf8(&self.oem_id).unwrap()
    }

    fn revision(&self) -> u8 {
        self.revision
    }

    fn rsdt_address(&self) -> u32 {
        self.rsdt_address
    }

    fn xsdt_address(&self) -> u64 {
        assert!(
            self.revision > 0,
            "Tried to read extended RSDP field with ACPI Version 1.0"
        );
        self.xsdt_address
    }
}

/// Find the areas we should search for the RSDP in.
fn find_search_areas() -> [Range<usize>; 2] {
    /*
     * Read the base address of the EBDA from its location in the BDA (BIOS Data Area). Not all BIOSs fill this out
     * unfortunately, so we might not get a sensible result. We shift it left 4, as it's a segment address.
     */
    let ebda_start_mapping = unsafe { *(EBDA_START_SEGMENT_PTR as *const u16) };
    let ebda_start = (ebda_start_mapping as usize) << 4;
    [
        /*
         * The main BIOS area below 1MiB. In practice, from my [Restioson's] testing, the RSDP is more often here
         * than the EBDA. We also don't want to search the entire possibele EBDA range, if we've failed to find it
         * from the BDA.
         */
        RSDP_BIOS_AREA_START..(RSDP_BIOS_AREA_END + 1),
        // Check if base segment ptr is in valid range for EBDA base
        if (EBDA_EARLIEST_START..EBDA_END).contains(&ebda_start) {
            // First KiB of EBDA
            ebda_start..ebda_start + 1024
        } else {
            // We don't know where the EBDA starts, so just search the largest possible EBDA
            EBDA_EARLIEST_START..(EBDA_END + 1)
        },
    ]
}

/// This (usually!) contains the base address of the EBDA (Extended Bios Data Area), shifted right by 4
const EBDA_START_SEGMENT_PTR: usize = 0x40e;
/// The earliest (lowest) memory address an EBDA (Extended Bios Data Area) can start
const EBDA_EARLIEST_START: usize = 0x80000;
/// The end of the EBDA (Extended Bios Data Area)
const EBDA_END: usize = 0x9ffff;
/// The start of the main BIOS area below 1mb in which to search for the RSDP (Root System Description Pointer)
const RSDP_BIOS_AREA_START: usize = 0xe0000;
/// The end of the main BIOS area below 1mb in which to search for the RSDP (Root System Description Pointer)
const RSDP_BIOS_AREA_END: usize = 0xfffff;
/// The RSDP (Root System Description Pointer)'s signature, "RSD PTR " (note trailing space)
const RSDP_SIGNATURE: &'static [u8; 8] = b"RSD PTR ";

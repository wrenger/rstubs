use core::arch::asm;

/// Specifies which element to load into a segment from
/// descriptor tables (i.e., is a index to LDT or GDT table
/// with some additional flags).
///
/// See Intel 3a, Section 3.4.2 "Segment Selectors"
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(transparent)]
pub struct SegmentSelector(pub u16);

impl SegmentSelector {
    pub const fn new(index: u16, user: bool) -> SegmentSelector {
        SegmentSelector(index << 3 | if user { 3 } else { 0 })
    }

    pub const fn index(self) -> u16 {
        self.0 >> 3
    }

    pub const fn user(self) -> bool {
        self.0 | 0b11 == 0b11
    }
}

/// Returns the current value of the code segment register.
#[inline]
pub fn code() -> SegmentSelector {
    let segment: u16;
    unsafe { asm!("mov {0:x}, cs", out(reg) segment, options(nostack, nomem)) };
    SegmentSelector(segment)
}

#![allow(dead_code)]

//! Configure legacy PIC 8259

use crate::arch::io::Port;

// PIC 1 config
const PIC1_OFFSET: u8 = 0x20;
const PIC1_CMD: Port<u8> = Port::new(0x20);
const PIC1_DATA: Port<u8> = Port::new(0x21);
// PIC 2 config
const PIC2_OFFSET: u8 = 0x28;
const PIC2_CMD: Port<u8> = Port::new(0xa0);
const PIC2_DATA: Port<u8> = Port::new(0xa1);
/// Command sent to begin PIC initialization.
const PIC_CMD_INIT: u8 = 0x11;
/// Command sent to acknowledge an interrupt.
const PIC_CMD_EOI: u8 = 0x20;
/// 8086/8088 or 8085 mode.
const PIC_MODE_8086: u8 = 0x01;
/// Single or multiple (cascade mode)  8259A
const PIC_MODE_AUTO_EOI: u8 = 0x02;

bitflags::bitflags! {
    pub struct PicPort: u16 {
        const NONE =          0;
        const TIMER =         1 << 0;
        const KEYBOARD =      1 << 1;
        const SERIAL1 =       1 << 3;
        const SERIAL2 =       1 << 4;
        const PARALLEL23 =    1 << 5;
        const FLOPPY =        1 << 6;
        const PARALLEL1 =     1 << 7;
        const RTC =           1 << 8;
        const ACPI =          1 << 9;
        const P10 =           1 << 10;
        const P11 =           1 << 11;
        const MOUSE =         1 << 12;
        const CO_PROCESSOR =  1 << 13;
        const PRIMART_ATA =   1 << 14;
        const SECONDARY_ATA = 1 << 15;
    }
}

/// Initialize the chained 8259 PICs as shown below.
///
/// Where `enable` specifies which interrupts are enabled.
/// E.g. `0b0000_0000_0000_0011` masks all interrupts except Timer and Keyboard.
///
/// ```text
///                      ____________                          ____________
/// Real Time Clock --> | 40         |   Timer -------------> | 32         |
/// ACPI -------------> |            |   Keyboard-----------> |            |
/// Available --------> | Secondary  |----------------------> | Primary    |
/// Available --------> | Interrupt  |   Serial Port 2 -----> | Interrupt  |->
/// Mouse ------------> | Controller |   Serial Port 1 -----> | Controller |
/// Co-Processor -----> |            |   Parallel Port 2/3 -> |            |
/// Primary ATA ------> |            |   Floppy disk -------> |            |
/// Secondary ATA ----> |_47_________|   Parallel Port 1----> |_39_________|
/// ```
pub fn init(enable: PicPort) {
    unsafe {
        // ICW1: 8086 mode with ICW4
        PIC1_CMD.write(PIC_CMD_INIT);
        PIC2_CMD.write(PIC_CMD_INIT);

        // ICW2 Master: IRQ # Offset (32)
        PIC1_DATA.write(PIC1_OFFSET);
        // ICW2 Slave: IRQ # Offset (40)
        PIC2_DATA.write(PIC2_OFFSET);

        // ICW3 Master: slaves on IRQs
        PIC1_DATA.write(0x04);
        // ICW3 Slave: connect to IRQ2 of the masters
        PIC2_DATA.write(0x02);

        //ICW4: 8086 mode
        PIC1_DATA.write(PIC_MODE_8086 | PIC_MODE_AUTO_EOI);
        PIC2_DATA.write(PIC_MODE_8086 | PIC_MODE_AUTO_EOI);

        // Set interrupt mask
        let mask = !enable.bits;
        PIC1_DATA.write(mask as u8);
        PIC2_DATA.write((mask >> 8) as u8);
    }
}

/// Set the End Of Interrupt bit to allow new interrupts.
pub fn eoi(vector: u8) {
    const PIC1_END: u8 = PIC1_OFFSET + 7;
    const PIC2_END: u8 = PIC2_OFFSET + 7;
    match vector {
        PIC1_OFFSET..=PIC1_END => unsafe { PIC1_CMD.write(PIC_CMD_EOI) },
        PIC2_OFFSET..=PIC2_END => unsafe { PIC2_CMD.write(PIC_CMD_EOI) },
        _ => {}
    }
}

pub fn disable() {
    unsafe {
        // select Interrupt Mode Control Register (IMCR)
        Port::<u8>::new(0x22).write(0x70);
        // disable PIC Mode
        Port::<u8>::new(0x23).write(0x01);
    }
}

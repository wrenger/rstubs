#![allow(dead_code)]

use core::mem::size_of;
use core::num::NonZeroUsize;
use core::sync::atomic::{AtomicPtr, Ordering};

use bitfield_struct::bitfield;

/// Abstraction of the I/O APIC that is used for management of external interrupts.
///
/// The I/O APIC's Core component is the IO-redirection table.
/// This table is used to configure a flexible mapping between
/// the interrupt number and the external interruption.
/// Entries within this table have a width of 64 bit.
pub struct IoApic {
    pub base: AtomicPtr<u32>,
}

impl IoApic {
    pub const BASE: usize = 0xfec0_0000;
    pub const SLOT_MAX: u8 = 24;

    pub const fn new() -> IoApic {
        IoApic {
            base: AtomicPtr::new(Self::BASE as _),
        }
    }
    /// Initializes the I/O APIC.
    ///
    /// This function will initialize the I/O APIC by initializing the
    /// IO-redirection table with sane default values.
    /// The default interrupt-vector number is chosen such that,
    /// in case the interrupt is issued, the panic handler is executed.
    /// In the beginning, all external interrupts are disabled within the I/O APIC.
    /// Apart from the redirection table, the `id`
    /// (read from the system description tables during boot) needs to be passed in.
    pub fn init(&self, base: Option<NonZeroUsize>, id: u8, panic_vector: u8) {
        if let Some(base) = base {
            self.base.store(base.get() as _, Ordering::Release);
        }

        // Set io apic id
        self.update(Registers::Id as u32, |v: Identification| v.with_id(id));

        let reg = IoApicVer(self.read(Registers::Version as u32));
        log!(
            "ioapic version: {:#x}, mre: {}",
            reg.version(),
            reg.max_redirection_entry()
        );

        for i in 0..Self::SLOT_MAX {
            self.config(i, panic_vector, false);
        }
    }

    /// Creates a mapping between an interrupt vector and an external interrupt.
    pub fn config(&self, slot: u8, vector: u8, trigger_level: bool) {
        assert!(slot < Self::SLOT_MAX);

        let mut reg_l: RedirectionTableEntryL = self.read(red_l(slot));
        let mut reg_h: RedirectionTableEntryH = self.read(red_h(slot));

        reg_l.set_vector(vector as u32);
        reg_l.set_delivery_mode(0x1); // lowest pri
        reg_l.set_destination_mode(true); // logical
        reg_l.set_polarity(false); // high
        reg_l.set_trigger_mode(trigger_level); // edge or level triggered
        reg_l.set_mask(true);

        reg_h.set_logical_destination(0b1);

        self.write(red_l(slot), reg_l);
        self.write(red_h(slot), reg_h);
    }

    pub fn allow(&self, slot: u8) {
        self.update(red_l(slot), |v: RedirectionTableEntryL| v.with_mask(false));
    }

    pub fn forbid(&self, slot: u8) {
        self.update(red_l(slot), |v: RedirectionTableEntryL| v.with_mask(true));
    }

    pub fn status(&self, slot: u8) -> bool {
        !self.read::<RedirectionTableEntryL>(red_l(slot)).mask()
    }

    fn write<T>(&self, addr: u32, reg: T) {
        unsafe {
            let base = self.base.load(Ordering::Acquire);
            base.write_volatile(addr);
            base.add(0x10 / size_of::<u32>())
                .cast::<T>()
                .write_volatile(reg);
        }
    }

    fn read<T>(&self, addr: u32) -> T {
        unsafe {
            let base = self.base.load(Ordering::Acquire);
            base.write_volatile(addr);
            base.add(0x10 / size_of::<u32>())
                .cast::<T>()
                .read_volatile()
        }
    }

    fn update<T>(&self, addr: u32, f: impl FnOnce(T) -> T) {
        unsafe {
            let base = self.base.load(Ordering::Acquire);
            let data = base.add(0x10 / size_of::<u32>()).cast::<T>();

            base.write_volatile(addr);
            let v = data.read_volatile();
            let v = f(v);
            data.write_volatile(v);
        }
    }
}

#[inline]
fn red_l(slot: u8) -> u32 {
    2 * (slot as u32) + 0x10
}

#[inline]
fn red_h(slot: u8) -> u32 {
    2 * (slot as u32) + 0x11
}

#[repr(u32)]
enum Registers {
    Id = 0,
    Version = 1,
}

// I/O APIC Identification
#[bitfield(u32)]
struct Identification {
    #[bits(24)]
    _p: u32,
    #[bits(4)]
    id: u8,
    #[bits(4)]
    _p: u32,
}

#[bitfield(u32)]
struct IoApicVer {
    #[bits(8)]
    version: u32,
    #[bits(8)]
    _p: u32,
    #[bits(8)]
    max_redirection_entry: u32,
    #[bits(8)]
    _p: u32,
}

/// Entry in the redirection table. Lower Half.
#[bitfield(u32)]
struct RedirectionTableEntryL {
    #[bits(8)]
    vector: u32,
    #[bits(3)]
    delivery_mode: u32,
    destination_mode: bool,
    delivery_status: bool,
    polarity: bool,
    remote_irr: bool,
    trigger_mode: bool,
    mask: bool,
    #[bits(15)]
    _p: u32,
}

/// Entry in the redirection table. Higher Half.
#[bitfield(u32)]
struct RedirectionTableEntryH {
    #[bits(24)]
    _p: u32,
    #[bits(8)]
    logical_destination: u32,
}

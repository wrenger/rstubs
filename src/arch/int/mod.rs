use core::arch::asm;

pub mod idt;
pub mod ioapic;
pub mod lapic;
pub mod pic;

#[inline]
pub fn dont_interrupt<T>(f: impl FnOnce() -> T) -> T {
    if enabled() {
        disable();
        let ret = f();
        enable();
        ret
    } else {
        f()
    }
}

#[inline]
pub fn enabled() -> bool {
    use crate::arch::registers::Flags;
    Flags::read().contains(Flags::INTERRUPT_FLAG)
}

#[inline]
pub fn enable() {
    unsafe { asm!("sti", options(nomem, nostack)) };
}

#[inline]
pub fn disable() {
    unsafe { asm!("cli", options(nomem, nostack)) };
}

#[inline]
pub fn enable_and_hlt() {
    unsafe { asm!("sti; hlt", options(nomem, nostack)) };
}

#[inline]
pub unsafe fn int3() {
    asm!("int3", options(nomem, nostack));
}

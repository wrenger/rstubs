#![allow(dead_code)]

use core::mem::transmute;
use core::num::NonZeroUsize;
use core::sync::atomic::{AtomicPtr, AtomicU32, Ordering};

use bitfield_struct::bitfield;

use crate::arch::io::{IOMem, VolatileUpdate};
use crate::arch::pit::Timer;

use super::dont_interrupt;

pub enum Device {
    /// Programmable Interrupt Timer
    Timer = 0,
    /// Keyboard
    Keyboard = 1,
    /// First serial interface
    Com1 = 4,
    /// Second serial interface
    Com2 = 3,
    /// Floppy device
    Floppy = 6,
    /// Printer
    LPT1 = 7,
    /// Real time clock
    RealTimeClock = 8,
    /// Mouse
    PS2Mouse = 12,
    /// First hard disk
    IDE1 = 14,
    /// Second hard disk
    IDE2 = 15,
}

/// Local APIC Implementation
///
/// See <https://nemez.net/osdev/lapic.txt>
/// See <https://wiki.osdev.org/APIC>
#[derive(Debug)]
pub struct LApic {
    /// Base of the memory mapped lapic registers
    pub base: AtomicPtr<u32>,
    pub timer_ticks: AtomicU32,
}

impl LApic {
    pub const BASE: usize = 0xfee0_0000;

    pub const fn new() -> LApic {
        LApic {
            base: AtomicPtr::new(Self::BASE as _),
            timer_ticks: AtomicU32::new(0),
        }
    }

    pub fn init(&self, cpu_id: u8, base: Option<NonZeroUsize>) {
        if let Some(base) = base {
            self.base.store(base.get() as *mut _, Ordering::Release);
        }

        log!("lapic: version {:#x}", self.version());

        // use 255 as spurious vector, enable APIC and disable focus processor
        self.update(|v: SVR| {
            v.with_spurious_vector(0xff)
                .with_apic_enable(true)
                .with_focus_processor_checking(true)
        });

        // set flat delivery mode
        self.update(|v: DFR| v.with_model(DESTINATION_MODEL_FLAT));

        // set task priority to 0 -> accept all interrupts
        self.update(|v: TPR| v.with_task_prio(0).with_task_prio_sub(0));

        // reset logical destination ID
        self.update(|v: LDR| v.with_lapic_id(1 << cpu_id));
    }

    pub fn set_logical_id(&self, id: u8) {
        self.update(|v: LDR| v.with_lapic_id(id as _));
    }

    pub fn eoi(&self) {
        // dummy read
        self.read::<SVR>();
        // signal end of interrupt
        self.write(EOI(0));
    }

    pub fn version(&self) -> u8 {
        self.read::<LapicVer>().version() as _
    }

    pub fn id(&self) -> u8 {
        self.read::<LapicID>().lapic_id() as _
    }

    pub fn ipi_delivered(&self) -> bool {
        !self.read::<IcrL>().delivery_status()
    }

    pub fn send_ipi(&self, dst: u8, data: IcrL) {
        while !self.ipi_delivered() {}

        self.update(|v: IcrH| v.with_destination(dst as _));
        self.write(data);
    }

    fn raw_timer(&self, counter: u32, divide: u32, vector: u8, periodic: bool, masked: bool) {
        use TimerMode::*;
        // stop timer
        self.write(ICR(0));
        // set control register
        self.update(|v: TimerCtrl| {
            v.with_vector(vector)
                .with_timer_mode(if periodic { Periodic } else { OneShot })
                .with_masked(masked)
        });
        // set divider
        self.write(DCR(divide));
        // start timer
        self.write(ICR(counter));
    }

    /// Setup the lapic timer
    pub fn timer(&self, us: u32, vector: u8, periodic: bool, masked: bool) {
        let mut ticks = self.timer_ticks.load(Ordering::Acquire);
        if ticks == 0 {
            ticks = self.calculate_ticks();
            self.timer_ticks.store(ticks, Ordering::Release);
        }
        log!("lapic: ticks {ticks}");
        let (counter, div) = us_to_counter(us, ticks);
        self.raw_timer(counter, lapic_div(div), vector, periodic, masked)
    }

    /// Determines the LAPIC timer divider.
    ///
    /// Steps taken for precise measurement of LAPIC-timer ticks per ms:
    /// 1. Disable Interrupts to ensure measurement is not disturbed
    /// 2. Configure a timeout of 50 ms (nearly PIT's maximum possible delay)
    ///    Using a "large" value decreases the overhead induced by measurement and thereby increases the accuracy.
    /// 3. Now measure the number of passed LAPIC-timer ticks while waiting for the PIT
    ///    Note that configuring the PIT takes quite some time and therefore should be done prior to starting
    ///    LAPIC-timer measurement.
    /// 4. Restore previous state (disable PIT, LAPIC timer, restore interrupts)
    /// 5. Derive the ticks per millisecond (take care, the counter is counting towards zero)
    fn calculate_ticks(&self) -> u32 {
        dont_interrupt(|| {
            const MS: u16 = 50;

            let pit = Timer::new(MS * 1000).unwrap();

            self.raw_timer(u32::MAX, 1, 0, false, true);

            let start = self.read::<CCR>();
            pit.wait();
            let end = self.read::<CCR>();

            self.raw_timer(0, 1, 0, false, true);

            ((start.0 - end.0) / MS as u32) as _
        })
    }

    fn enable_lapic() {
        use crate::arch::registers::Msr;
        const IA32_APIC_BASE_MSR: u32 = 0x1B;
        const IA32_APIC_BASE_MSR_ENABLE: u64 = 0x800;

        unsafe {
            let base = Msr(IA32_APIC_BASE_MSR).read();
            Msr(IA32_APIC_BASE_MSR).write(base | IA32_APIC_BASE_MSR_ENABLE);
        }
    }

    fn update<T: IOMem>(&self, f: impl FnOnce(T) -> T) {
        unsafe {
            self.base
                .load(Ordering::Acquire)
                .add(T::OFFSET / 4)
                .cast::<T>()
                .update_volatile(f)
        }
    }

    fn write<T: IOMem>(&self, value: T) {
        unsafe {
            self.base
                .load(Ordering::Acquire)
                .add(T::OFFSET / 4)
                .cast::<T>()
                .write_volatile(value)
        };
    }

    fn read<T: IOMem>(&self) -> T {
        unsafe {
            self.base
                .load(Ordering::Acquire)
                .add(T::OFFSET / 4)
                .cast::<T>()
                .read_volatile()
        }
    }
}

fn lapic_div(div: u8) -> u32 {
    let marks = [
        0xb, // divides by   1
        0x0, // divides by   2
        0x1, // divides by   4
        0x2, // divides by   8
        0x3, // divides by  16
        0x8, // divides by  32
        0x9, // divides by  64
        0xa, // divides by 128
    ];
    let trail = div.trailing_zeros();
    if div != 0 && div.is_power_of_two() && trail < marks.len() as u32 {
        marks[trail as usize]
    } else {
        0xff
    }
}

fn us_to_counter(us: u32, ticks: u32) -> (u32, u8) {
    assert!(us != 0);

    let l_counter = (ticks as u64 * us as u64) / 1000;
    // Number of bits exceeding the 32 bit boundary
    let overlap = 32u32.saturating_sub(l_counter.leading_zeros());
    let divider = 1u8 << overlap;
    // convert to 32 bit
    let counter = (l_counter / divider as u64) as u32;
    (counter, divider)
}

/// Register for reconfiguring the base address
#[bitfield(u64)]
struct APICBaseAddr {
    _p: u8,
    bootstrap_cpu: bool,
    #[bits(2)]
    _p: u8,
    enable: bool,
    #[bits(40)]
    base_addr: u64,
    #[bits(12)]
    _p: u64,
}

/// Local APIC ID Register, R/W
#[bitfield(u32)]
struct LapicID {
    #[bits(24)]
    _p: u64,
    lapic_id: u8,
}
impl IOMem for LapicID {
    const OFFSET: usize = 0x020;
}

/// Local APIC Version Register, RO
#[bitfield(u32)]
struct LapicVer {
    version: u8,
    _p: u8,
    mle: u8,
    #[bits(7)]
    _p: u8,
    eas: bool,
}
impl IOMem for LapicVer {
    const OFFSET: usize = 0x030;
}

/// Task Priority Register, R/W
#[bitfield(u32)]
struct TPR {
    #[bits(4)]
    task_prio_sub: u8,
    #[bits(4)]
    task_prio: u8,
    #[bits(24)]
    _p: u32,
}
impl IOMem for TPR {
    const OFFSET: usize = 0x080;
}

/// EOI Register, WO
#[repr(transparent)]
struct EOI(u32);
impl IOMem for EOI {
    const OFFSET: usize = 0x0b0;
}

/// Logical Destination Register, R/W
#[bitfield(u32)]
struct LDR {
    #[bits(24)]
    _p: u32,
    lapic_id: u8,
}
impl IOMem for LDR {
    const OFFSET: usize = 0x0d0;
}

const DESTINATION_MODEL_CLUSTER: u8 = 0x0;
const DESTINATION_MODEL_FLAT: u8 = 0xf;
/// Destination Format Register, bits 0-27 RO, bits 28-31 R/W
#[bitfield(u32)]
struct DFR {
    #[bits(28)]
    _p: u32,
    #[bits(4)]
    model: u8,
}
impl IOMem for DFR {
    const OFFSET: usize = 0x0e0;
}

/// Spurious Interrupt Vector Register, bits 0-8 R/W, bits 9-1 R/W
#[bitfield(u32)]
struct SVR {
    spurious_vector: u8,
    apic_enable: bool,
    focus_processor_checking: bool,
    #[bits(22)]
    _p: u32,
}
impl IOMem for SVR {
    const OFFSET: usize = 0x0f0;
}

/// Interrupt Command Register 1, R/W
#[bitfield(u32)]
pub struct IcrL {
    pub vector: u8,
    #[bits(3)]
    pub message_type: u8,
    pub destination_mode: bool,
    pub delivery_status: bool,
    _p: bool,
    pub level: bool,
    pub trigger_mode: bool,
    #[bits(2)]
    pub remote_read_status: u8,
    #[bits(2)]
    pub destination_shorthand: u8,

    // ...
    #[bits(12)]
    _p: u32,
}
impl IOMem for IcrL {
    const OFFSET: usize = 0x300;
}

/// Interrupt Command Register 2, R/W
#[bitfield(u32)]
struct IcrH {
    #[bits(24)]
    _p: u32,
    destination: u8,
}
impl IOMem for IcrH {
    const OFFSET: usize = 0x310;
}

/// LApic timer control register, R/W
#[bitfield(u32)]
struct TimerCtrl {
    vector: u8,
    #[bits(4)]
    _p: u8,
    delivery_status: bool,
    #[bits(3)]
    _p: u8,
    masked: bool,
    #[bits(2)]
    timer_mode: TimerMode,
    #[bits(13)]
    _p: u32,
}
impl IOMem for TimerCtrl {
    const OFFSET: usize = 0x320;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
enum TimerMode {
    OneShot = 0,
    Periodic = 1,
    Deadline = 2,
    _R = 3,
}
impl From<u32> for TimerMode {
    fn from(value: u32) -> Self {
        unsafe { transmute(value) }
    }
}
impl From<TimerMode> for u32 {
    fn from(value: TimerMode) -> Self {
        value as _
    }
}

/// LApic timer initial counter register, R/W
#[repr(transparent)]
struct ICR(u32);
impl IOMem for ICR {
    const OFFSET: usize = 0x380;
}

/// LApic timer current counter register, RO
#[repr(transparent)]
struct CCR(u32);
impl IOMem for CCR {
    const OFFSET: usize = 0x390;
}

/// LApic timer divide configuration register, RW
#[repr(transparent)]
struct DCR(u32);
impl IOMem for DCR {
    const OFFSET: usize = 0x3e0;
}

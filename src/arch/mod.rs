use core::arch::asm;

pub mod acpi;
pub mod descriptors;
pub mod int;
pub mod io;
pub mod paging;
pub mod pit;
pub mod registers;
pub mod segmentation;
pub mod multiboot;
pub mod tss;

#[inline]
pub fn hlt() {
    unsafe { asm!("hlt", options(nomem, nostack)) };
}

#[inline]
pub fn nop() {
    unsafe { asm!("nop", options(nomem, nostack)) };
}

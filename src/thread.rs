use core::arch::asm;
use core::fmt;

use crate::interrupts::guard::Guard;

#[derive(Debug, Copy, Clone)]
#[repr(packed)]
#[repr(C)]
/// Non-volatile registers that have to survive function calls.
///
/// See <https://wiki.osdev.org/Calling_Conventions>
pub struct Registers {
    ebx: u32,
    esi: u32,
    edi: u32,
    ebp: u32,
    esp: u32,
}

impl Registers {
    pub const fn new() -> Registers {
        Registers {
            ebx: 0,
            esi: 0,
            edi: 0,
            ebp: 0,
            esp: 0,
        }
    }
}

pub struct Thread {
    pub id: usize,
    registers: Registers,
    stack: [u8; 8 * 1024],
    pub action: fn(&Thread),
    pub next: Option<&'static mut Thread>,
}

impl fmt::Debug for Thread {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Thread")
            .field("id", &self.id)
            .field("registers", &self.registers)
            .field("next", &self.next)
            .finish()
    }
}

impl Thread {
    pub const fn new(id: usize, action: fn(&Thread)) -> Thread {
        Thread {
            id,
            registers: Registers::new(),
            stack: [0; 8 * 1024],
            action,
            next: None,
        }
    }

    pub fn run(&mut self) {
        (self.action)(self);
    }

    pub fn init(&mut self)
    where
        Self: core::marker::Sized,
    {
        let reference = self as *mut Thread;

        // Setup stack
        let stack = &mut self.stack;
        let mut rsp = unsafe { stack.as_ptr().add(stack.len()) } as *mut u32;
        unsafe {
            rsp = rsp.sub(1);
            *rsp = reference as u32; // arg1
            rsp = rsp.sub(1);
            *rsp = 0xbeefcace; // old rbp
            rsp = rsp.sub(1);
            *rsp = Thread::kickoff as *mut () as u32; // ret
        }

        // Setup registers
        let registers = &mut self.registers;
        registers.esp = rsp as u32;
        registers.ebp = 0xbeefcace;
    }

    pub fn resume(&mut self, next: &mut Thread) {
        unsafe { toc_switch_rs(&mut self.registers, &next.registers) };
    }

    pub fn go(&mut self) {
        unsafe { toc_go_rs(&self.registers) };
    }

    pub fn next(&mut self) -> &mut Option<&'static mut Thread> {
        &mut self.next
    }

    fn kickoff(thread: &mut Thread) {
        Guard::leave();

        thread.run();
    }
}

/// Starts the thread for the first time
unsafe fn toc_go_rs(thread: &Registers) {
    asm!("
        mov ebx,[eax+0]
        mov esi,[eax+4]
        mov edi,[eax+8]
        mov ebp,[eax+12]
        mov esp,[eax+16]
        ",
        in("eax") thread, // Scratch Register
        options(nostack)
    )
}

/// Switches between two threads
unsafe fn toc_switch_rs(current: &mut Registers, next: &Registers) {
    asm!("
        mov [eax+0],ebx
        mov [eax+4],esi
        mov [eax+8],edi
        mov [eax+12],ebp
        mov [eax+16],esp

        mov ebx,[ecx+0]
        mov esi,[ecx+4]
        mov edi,[ecx+8]
        mov ebp,[ecx+12]
        mov esp,[ecx+16]
        ",
        in("eax") current, // Scratch Register
        in("ecx") next,    // Scratch Register
        options(nostack)
    )
}

use core::{
    fmt,
    ops::{BitAnd, BitOr, Range},
};

use crate::arch::io::Port;

const INDEX: Port<u8> = Port::new(0x3d4);
const DATA: Port<u8> = Port::new(0x3d5);
// virtual addr 3GiB is mapped to 0GiB
const BUFFER: *mut Cell = 0xb8000 as *mut Cell;
const BOUNDS: Rect = Rect::new(0, 0, 80, 25);

/// The screen represents the cga screen or a part of it.
///
/// There can be multiple screes to different areas of the cga.
/// However, only one of them may controll the hw_cursor at the same time.
#[derive(Debug)]
pub struct Screen {
    cursor: (u8, u8),
    hw_cursor: bool,
    rect: Rect,
}

/// The kernel output screen.
/// Access to it has to be synchronized.
pub static mut KOUT: Screen = Screen::new(Rect::new(0, 4, 40, 21)).with_hw_cursor();
/// The debug output screen.
pub static mut DBG: Screen = Screen::new(Rect::new(40, 0, 40, 25));

#[macro_export]
macro_rules! kprint {
    ($($arg:tt)*) => ($crate::cga::_kprint(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! kprintln {
    () => ($crate::kprint!("\n"));
    ($($arg:tt)*) => ($crate::kprint!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _kprint(args: fmt::Arguments) {
    use core::fmt::Write;
    unsafe { KOUT.write_fmt(args).unwrap() };
}

#[macro_export]
macro_rules! dprint {
    ($($arg:tt)*) => ($crate::cga::_dprint(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! dprintln {
    () => ($crate::dprint!("\n"));
    ($($arg:tt)*) => ($crate::dprint!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _dprint(args: fmt::Arguments) {
    use core::fmt::Write;
    unsafe { DBG.write_fmt(args).unwrap() };
}

impl Screen {
    pub const fn whole() -> Screen {
        Self::new(BOUNDS)
    }
    pub const fn new(rect: Rect) -> Screen {
        let rect = rect & BOUNDS;
        Screen {
            cursor: (rect.x, rect.y + rect.h - 1),
            hw_cursor: false,
            rect,
        }
    }

    pub const fn with_hw_cursor(mut self) -> Self {
        self.hw_cursor = true;
        self
    }

    pub fn clear(&mut self) {
        self.fill(Cell::clear(Attribute::default()));
    }

    pub fn fill(&mut self, cell: Cell) {
        for y in self.rect.rows() {
            for x in self.rect.cols() {
                self.write_cell((x, y), cell);
            }
        }
    }

    pub fn write_cell(&mut self, pos: (u8, u8), cell: Cell) {
        let i = pos.0 as usize + pos.1 as usize * VGA_COLUMNS;
        if i < VGA_ROWS * VGA_COLUMNS {
            unsafe { BUFFER.add(i).write_volatile(cell) };
        }
    }

    fn get_cell(&self, pos: (u8, u8)) -> Cell {
        let i = pos.0 as usize + pos.1 as usize * VGA_COLUMNS;
        if i < VGA_ROWS * VGA_COLUMNS {
            unsafe { BUFFER.add(i).read_volatile() }
        } else {
            Cell::clear(Attribute::default())
        }
    }

    pub fn set_cursor(&mut self, pos: (u8, u8)) {
        self.cursor = pos;
        if self.hw_cursor {
            let i = pos.0 as u16 + pos.1 as u16 * VGA_COLUMNS as u16;
            unsafe {
                INDEX.write(14);
                DATA.write((i >> 8) as u8);
                INDEX.write(15);
                DATA.write(i as u8);
            }
        } else {
            self.write_cell(
                pos,
                Cell::new(b'_', Attribute::new(Color::White, Color::Black, true)),
            )
        }
    }

    pub fn get_cursor(&mut self) -> (u8, u8) {
        self.cursor
    }

    pub fn style(&mut self, attr: Attribute) -> StyledScreen<'_> {
        StyledScreen { screen: self, attr }
    }
}

impl fmt::Write for Screen {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.style(Attribute::default()).write_str(s)
    }
}

/// A temporary screen that allows styling the printed output.
pub struct StyledScreen<'a> {
    screen: &'a mut Screen,
    attr: Attribute,
}

impl<'a> fmt::Write for StyledScreen<'a> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let (mut x, mut y) = self.screen.cursor;
        let rect = self.screen.rect;
        for byte in s.bytes() {
            match byte {
                b'\n' => {
                    for xi in x..rect.x + rect.w {
                        self.screen.write_cell((xi, y), Cell::new(b' ', self.attr));
                    }
                    x = rect.x + rect.w - 1;
                }
                _ => self.screen.write_cell((x, y), Cell::new(byte, self.attr)),
            }

            if x < rect.x + rect.w - 1 {
                x += 1;
            } else if y < rect.y + rect.h - 1 {
                x = rect.x;
                y += 1;
            } else {
                x = rect.x;
                y = rect.y + rect.h - 1;
                // Move lines up
                for ny in rect.y..y {
                    for nx in rect.cols() {
                        self.screen
                            .write_cell((nx, ny), self.screen.get_cell((nx, ny + 1)));
                    }
                }
                // Move lines up
                for nx in rect.cols() {
                    self.screen.write_cell((nx, y), Cell::new(b' ', self.attr));
                }
            }
        }
        self.screen.set_cursor((x, y));

        Ok(())
    }
}

/// Display Color for the foreground and background of a vga cell
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
#[allow(dead_code)]
pub enum Color {
    Black = 0,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    LightGrey,
    DarkGrey,
    LightBlue,
    LightGreen,
    LightCyan,
    LightRed,
    LightMagenta,
    Yellow,
    White,
}

const VGA_ROWS: usize = 25;
const VGA_COLUMNS: usize = 80;

/// Defines the style a cga character including its background and foreground colors.
#[derive(Debug, Clone, Copy)]
#[repr(packed)]
pub struct Attribute(u8);

impl const Default for Attribute {
    fn default() -> Attribute {
        Attribute::new(Color::LightGrey, Color::Black, false)
    }
}

impl Attribute {
    pub const fn new(fg: Color, bg: Color, blink: bool) -> Attribute {
        Attribute((fg as u8) | (bg as u8 & 0b111) << 4 | (blink as u8) << 7)
    }
}

/// Representation of a cga screen cell, that consists of a character and its styling.
#[derive(Debug, Clone, Copy)]
#[repr(packed)]
#[allow(dead_code)]
pub struct Cell {
    character: u8,
    attr: Attribute,
}

impl Cell {
    pub const fn new(character: u8, attr: Attribute) -> Cell {
        Cell { character, attr }
    }

    pub const fn clear(attr: Attribute) -> Cell {
        Cell::new(b' ', attr)
    }
}

/// Defines the rect of a Screen.
/// The maximum cga boundaries are (0, 0, 80, 25).
#[derive(Debug, Clone, Copy)]
#[repr(packed)]
pub struct Rect {
    pub x: u8,
    pub y: u8,
    pub w: u8,
    pub h: u8,
}

impl Rect {
    pub const fn new(x: u8, y: u8, w: u8, h: u8) -> Rect {
        Rect { x, y, w, h }
    }
    pub const fn cols(self) -> Range<u8> {
        self.x..self.x + self.w
    }
    pub const fn rows(self) -> Range<u8> {
        self.y..self.y + self.h
    }
}

impl From<(u8, u8, u8, u8)> for Rect {
    fn from(v: (u8, u8, u8, u8)) -> Self {
        Rect::new(v.0, v.1, v.2, v.3)
    }
}

impl const BitAnd for Rect {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        let x = max(self.x, rhs.x);
        let y = max(self.y, rhs.y);
        let lx = min(self.x + self.w, rhs.x + rhs.w);
        let ly = min(self.y + self.h, rhs.y + rhs.h);
        Self::new(x, y, lx.saturating_sub(x), ly.saturating_sub(y))
    }
}
impl const BitOr for Rect {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        let x = min(self.x, rhs.x);
        let y = min(self.y, rhs.y);
        let lx = max(self.x + self.w, rhs.x + rhs.w);
        let ly = max(self.y + self.h, rhs.y + rhs.h);
        Self::new(x, y, lx.saturating_sub(x), ly.saturating_sub(y))
    }
}
const fn max(a: u8, b: u8) -> u8 {
    if a < b {
        b
    } else {
        a
    }
}
const fn min(a: u8, b: u8) -> u8 {
    if a < b {
        a
    } else {
        b
    }
}

use crate::arch::descriptors::{self, Descriptor, DescriptorTablePointer};
use crate::arch::tss::TaskStateSegment;

#[repr(C)]
#[repr(align(0x1000))]
struct IntStack([u8; 0x1000]);

static INT_STACK: IntStack = IntStack([0; 0x1000]);
static mut TSS: TaskStateSegment = TaskStateSegment::new(0, 0);

static mut GDT: [Descriptor; 6] = [
    Descriptor::null(),
    Descriptor::kernel_code(),
    Descriptor::kernel_data(),
    Descriptor::user_code(),
    Descriptor::user_data(),
    Descriptor::null(), // Placeholder for tss
];

#[no_mangle]
pub static mut GDT_PTR: DescriptorTablePointer = DescriptorTablePointer {
    limit: unsafe { (core::mem::size_of_val(&GDT) - 1) as u16 },
    base: unsafe { GDT.as_mut_ptr() },
};

/// Initialize global descriptor table & setup segments
///
/// Called from start.s assembly
#[no_mangle]
#[cfg(target_arch = "x86")]
pub extern "C" fn setup_gdt() -> *const DescriptorTablePointer {
    unsafe {
        TSS.esp0 = INT_STACK.0.as_ptr() as u32;
        let tss_desc = Descriptor::tss(&TSS);
        log!("TSS: {tss_desc:?}");
        let sel = descriptors::set(&GDT_PTR, 5, tss_desc);
        log!("{sel:?}");

        &GDT_PTR as *const _
    }
}

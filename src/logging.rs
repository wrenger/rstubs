use core::fmt;
use core::sync::atomic::{AtomicBool, Ordering};

use crate::arch::io::Port;

/// Simple wrapper for printing hex values
pub struct Hex<T>(pub T);
impl<T: fmt::LowerHex> fmt::Debug for Hex<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "0x{:#x}", self.0)
    }
}

/// A formatter object
pub struct Writer(bool);

/// A primitive lock for the logging output
///
/// This is not really a lock. Since there is no threading at the moment, all
/// it does is prevent writing when a collision would occur.
static LOGGING_LOCK: AtomicBool = AtomicBool::new(false);

impl Writer {
    /// Obtain a logger for the specified module
    pub fn get() -> Writer {
        // This "acquires" the lock (actually just disables output if parallel writes are attempted
        Writer(!LOGGING_LOCK.swap(true, Ordering::Acquire))
    }
}

impl ::core::ops::Drop for Writer {
    fn drop(&mut self) {
        // Write a terminating newline before releasing the lock
        use core::fmt::Write;
        let _ = writeln!(self);
        // On drop, "release" the lock
        if self.0 {
            LOGGING_LOCK.store(false, Ordering::Release);
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        // If the lock is owned by this instance, then we can safely write to the output
        if self.0 {
            unsafe { puts(s) };
        }
        Ok(())
    }
}

/// A very primitive logging macro
///
/// Obtains a logger instance (locking the log channel) with the current module name passed
/// then passes the standard format! arguments to it
macro_rules! log{
	( $($arg:tt)* ) => ({
		// Import the Writer trait (required by write!)
		use core::fmt::Write;
		let _ = write!(&mut crate::logging::Writer::get(), $($arg)*);
	})
}

/// Write a string to the output channel
///
/// This method is unsafe because it does port accesses without synchronisation
unsafe fn puts(s: &str) {
    for b in s.bytes() {
        putb(b);
    }
}

/// Write a single byte to the output channel
///
/// This method is unsafe because it does port accesses without synchronisation
unsafe fn putb(b: u8) {
    const COM1: Port<u8> = Port::new(0x3F8);

    // Wait for the serial port's fifo to not be empty
    while (COM1 + 5).read() & 0x20 == 0 {}

    // Send the byte out the serial port
    COM1.write(b);
}

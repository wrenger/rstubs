use crate::thread::Thread;

static mut QUEUE: Queue = Queue::new();
static mut ACTIVE: Option<*mut Thread> = None;

/// Start the scheduling.
/// This function does not return.
pub fn schedule() -> ! {
    if let Some(first) = unsafe { QUEUE.dequeue() } {
        dispatch(first);
    }
    panic!("Nothing to be scheduled!");
}

/// Registers a thread to be executed by the scheduler.
pub fn ready(t: &'static mut Thread) {
    unsafe { QUEUE.enqueue(t) };
}

/// Initiates a thread switch.
/// This function returns then in the context of the next thread.
pub fn resume() {
    if let Some(active) = unsafe { ACTIVE } {
        unsafe { QUEUE.enqueue(&mut *active) };

        if let Some(next) = unsafe { QUEUE.dequeue() } {
            dispatch(next);
        }
    }
}

/// Terminates the current active thread.
pub fn exit() {
    if let Some(next) = unsafe { QUEUE.dequeue() } {
        dispatch(next);
    }
}

/// Terminates `t`.
pub fn kill(t: &'static mut Thread) {
    if let Some(active) = unsafe { ACTIVE } {
        if active == t {
            exit();
        }
    }
    unsafe { QUEUE.remove(t) };
}

fn dispatch(t: &'static mut Thread) {
    unsafe {
        if let Some(last) = ACTIVE.replace(t) {
            (*last).resume(&mut *ACTIVE.unwrap());
        } else {
            (*ACTIVE.unwrap()).go();
        }
    }
}

#[derive(Debug)]
struct Queue {
    start: Option<*mut Thread>,
    end: Option<*mut Thread>,
}

impl Queue {
    const fn new() -> Queue {
        Queue {
            start: None,
            end: None,
        }
    }

    fn enqueue(&mut self, item: &'static mut Thread) {
        let item = item as *mut Thread;
        if let Some(end) = self.end {
            unsafe { *((*end).next()) = Some(&mut *item) };
            self.end = Some(item);
        } else {
            self.start = Some(item);
            self.end = Some(item);
        }
    }

    fn dequeue(&mut self) -> Option<&'static mut Thread> {
        if let Some(start) = self.start {
            self.start = unsafe { (*start).next().take().map(|e| e as *mut _) };
            if self.start.is_none() {
                self.end = None;
            }
            Some(unsafe { &mut *start })
        } else {
            None
        }
    }

    fn remove(&mut self, item: &'static mut Thread) -> bool {
        let mut next = self.start;
        let mut last: Option<*mut Thread> = None;
        while let Some(current) = next {
            if current == item {
                if let Some(last) = last {
                    unsafe { *(*last).next() = (*current).next().take() };
                } else {
                    self.start = unsafe { (*current).next().take().map(|i| i as *mut _) };
                }
                return true;
            }

            last = Some(current);
            next = unsafe { (*current).next().as_mut().map(|i| *i as *mut _) };
        }
        false
    }
}

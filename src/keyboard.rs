use pc_keyboard::{layouts, DecodedKey, HandleControl, KeyCode, KeyEvent, KeyState, ScancodeSet1};

use crate::arch::io::Port;

bitflags::bitflags! {
    pub struct Led: u8 {
        const CAPS_LOCK = 0b0100;
        const SCROLL_LOCK = 0b0010;
        const NUM_LOCK = 0b0001;
    }
}

const CTRL: Port<u8> = Port::new(0x64);
const DATA: Port<u8> = Port::new(0x60);
const CMOS_CTRL: Port<u8> = Port::new(0x70);
const CMOS_DATA: Port<u8> = Port::new(0x71);
const SYS_CTRL_A: Port<u8> = Port::new(0x92);

#[repr(u8)]
enum StatusBit {
    Outb = 0x01,
    Inpb = 0x02,
    Auxb = 0x20,
}

pub struct Keyboard {
    inner: Option<pc_keyboard::Keyboard<layouts::Us104Key, ScancodeSet1>>,
    leds: Led,
    ctrl: bool,
}

pub static mut KEYBOARD: Keyboard = Keyboard::new();

impl Keyboard {
    pub const fn new() -> Keyboard {
        Keyboard {
            inner: None,
            leds: Led::empty(),
            ctrl: false,
        }
    }

    pub fn init(&mut self) {
        self.set_led(Led::CAPS_LOCK | Led::SCROLL_LOCK | Led::NUM_LOCK, false);
        self.set_repeat_rate(0, 0);
        self.inner = Some(pc_keyboard::Keyboard::new(
            layouts::Us104Key,
            ScancodeSet1,
            HandleControl::Ignore,
        ));
    }

    pub fn set_led(&mut self, led: Led, on: bool) {
        if on {
            self.leds |= led;
        } else {
            self.leds -= led;
        }
        self.send_cmd(0xed, self.leds.bits);
    }

    pub fn set_repeat_rate(&mut self, speed: u8, delay: u8) {
        if speed <= 31 && delay <= 3 {
            self.send_cmd(0xf3, delay << 4 | speed);
        }
    }

    pub fn key_hit(&mut self) -> Option<char> {
        let event = self.key_event()?;
        match event {
            KeyEvent {
                code: KeyCode::ControlLeft | KeyCode::ControlRight,
                state,
            } => self.ctrl = state == KeyState::Down,
            KeyEvent {
                code: KeyCode::Delete,
                state: KeyState::Down,
            } if self.ctrl => {
                dprintln!("\nreboot");
                self.reboot();
            }
            _ => {}
        }
        self.inner
            .as_mut()?
            .process_keyevent(event)
            .and_then(|c| match c {
                DecodedKey::RawKey(_) => None,
                DecodedKey::Unicode(c) => Some(c),
            })
    }

    fn key_event(&mut self) -> Option<KeyEvent> {
        let status = unsafe { CTRL.read() };
        if status & StatusBit::Outb as u8 != 0 {
            let scan = unsafe { DATA.read() };
            if status & StatusBit::Auxb as u8 == 0 {
                return self.inner.as_mut()?.add_byte(scan).ok()?;
            }
        }
        None
    }

    pub fn reboot(&self) {
        unsafe {
            CMOS_CTRL.write(0xe + 1);
            CMOS_DATA.write(0);
            SYS_CTRL_A.write(0x3);
        }
    }

    fn send_cmd(&mut self, cmd: u8, data: u8) {
        unsafe {
            while CTRL.read() & StatusBit::Inpb as u8 != 0 {}
            CTRL.write(cmd);
            CTRL.write(data);
        }
    }
}

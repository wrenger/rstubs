use crate::cga::KOUT;
use crate::interrupts::guard::Guard;
use crate::keyboard::KEYBOARD;
use crate::thread::Thread;

pub fn app_action(t: &Thread) {
    let mut i = 0_u16;
    loop {
        {
            let _guard = Guard::enter();
            let screen = unsafe { &mut KOUT };
            let tmp = screen.get_cursor();
            screen.set_cursor((0, t.id as u8));
            kprint!("i: {i}");
            screen.set_cursor(tmp);
        };
        i = i.wrapping_add(1);
    }
}

#[allow(unused)]
pub fn keyboard_action(_t: &Thread) {
    let keyboard = unsafe { &mut KEYBOARD };
    loop {
        if let Some(c) = keyboard.key_hit() {
            kprint!("{c}");
        }
    }
}

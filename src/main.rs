#![no_std] // don't link the Rust standard library
#![no_main] // disable all Rust-level entry points

// unstable features
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]
#![feature(const_size_of_val)]
#![feature(const_trait_impl)]
#![feature(lang_items)]
// custom tests
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![allow(bad_asm_style)]

//! Rust version of the MPStuBS

use core::panic::PanicInfo;
use core::{arch::global_asm, mem::size_of};

#[macro_use]
mod cga;

#[macro_use]
mod logging;

mod alloc;
mod app;
mod arch;
mod gdt;
mod interrupts;
mod keyboard;
mod paging;
mod scheduler;
mod thread;

use app::{app_action, keyboard_action};
use cga::Screen;
use keyboard::KEYBOARD;
use thread::Thread;

use arch::acpi;

use crate::paging::{KERNEL_MEM_BEGIN, KERNEL_MEM_END};

const CPUS: usize = 1;

const _: () = assert!(size_of::<*const ()>() == size_of::<usize>());

global_asm!(include_str!("arch/i686/start.s"), options(raw));

extern "C" {
    // Setup gdt and its pointer: has to be defined in rust
    #[allow(unused)]
    fn setup_gdt() -> *const arch::descriptors::DescriptorTablePointer;

    static MBOOT_SIG: u32;
    static MBOOT_PTR: *mut ();

    static kernel_begin: *mut ();
    static kernel_end: *mut ();
}

static mut THREADS: [Thread; 8] = [
    Thread::new(0, app_action),
    Thread::new(1, app_action),
    Thread::new(2, app_action),
    Thread::new(3, app_action),
    Thread::new(4, app_action),
    Thread::new(5, app_action),
    Thread::new(6, app_action),
    Thread::new(10, keyboard_action),
];

#[no_mangle]
pub fn kmain() -> ! {
    log!("kernel init");

    log!("code: {:x?}", unsafe { kernel_begin..kernel_end });
    log!("memory: {KERNEL_MEM_BEGIN:08x}..{KERNEL_MEM_END:08x}");

    let acpi = acpi::init().unwrap();

    Screen::whole().clear();
    unsafe { KEYBOARD.init() };

    dprintln!("int starting...");
    log!("gdt init");
    interrupts::init(acpi);

    let pd = paging::setup_paging();
    arch::paging::set_pd(pd);
    arch::paging::enable();
    log!("paging enabled");

    kprintln!("Hello World!");

    log!("int init");
    arch::int::enable();
    log!("int enabled...");

    let multiboot = arch::multiboot::load(unsafe {MBOOT_SIG}, unsafe { MBOOT_PTR });

    log!(
        "Multiboot command-line: {:?}",
        multiboot.command_line().unwrap_or_default()
    );

    for thread in unsafe { &mut THREADS } {
        thread.init();
        scheduler::ready(thread);
    }

    log!("Scheduler starting...");
    scheduler::schedule();
}

/// This function is called on panic.
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    log!("{info}");
    loop {
        arch::int::disable();
        arch::hlt();
    }
}

#[cfg(test)]
fn test_runner(tests: &[&dyn Fn()]) {
    use crate::arch::io::Port;

    const QEMU_EXIT: Port<u32> = Port::new(0xf4);

    log!("Running {} tests", tests.len());
    for test in tests {
        test();
    }
    log!("Ok");
    unsafe { QEMU_EXIT.write(0x10) };
}

#[cfg(test)]
mod test {
    #[test_case]
    fn test() {
        kprintln!("Hello from test!");
        log!("Hello from test!");
    }
}

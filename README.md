# RStuBS

Rust version of the MPStuBS OS kernel: https://www.sra.uni-hannover.de.

Additionally inspired by:
* https://os.phil-opp.com/minimal-rust-kernel/
* https://github.com/thepowersgang/rust-barebones-kernel
* http://www.randomhacks.net/bare-metal-rust/

Tasks:
* [x] Linker config and startup code.
* [x] CGA Output & Keyboard
* [x] Interrupt handling
* [x] Prologue/Epilogue
* [x] Thread switching (cooperative scheduling)
* [x] Timer & preemptive scheduling
* [ ] Passive waiting (mutex, sema)
* [x] Setup paging
* [x] Kernel allocator
* [ ] User allocator
* [ ] User applications

TODOs:
* Using APIC instead of PIC 8259
* Multicore?

## Dependencies

> Warning: Rust nightly for i686 is needed!

```bash
rustup component add rust-src # used for build-std
```

## Build

The cargo build system is used to build and link the kernel.
The following command builds the kernel and runs it with qemu.

```bash
cargo run
```

This uses the qemu i368 runner specified in [.cargo/config.toml](.cargo/config.toml).
It is similar to the following commands.

```bash
cargo build && qemu-system-i386 -serial stdio -gdb 'tcp::1234' -no-shutdown -no-reboot -kernel target/i686-unknown-rstubs/debug/rstubs
```
